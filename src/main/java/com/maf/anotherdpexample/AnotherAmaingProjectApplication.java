package com.maf.anotherdpexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnotherAmaingProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnotherAmaingProjectApplication.class, args);
	}

}
